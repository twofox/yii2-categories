<?php
/*
 * This file is part of the YiiModules.com
 *
 * (c) Yii2 modules open source project are hosted on <http://github.com/twofox/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace twofox\categories\models;
use Yii;
use yii\imagine\Image;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\helpers\Inflector;
use yii\helpers\Html;
/**
 * This is the model class for table "ymd_categories".
 *
 * @property string $id
 * @property string $parent_id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $image
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $position
 * @property integer $is_active
 * @property integer $created_at
 * @property integer $updated_at
 */
class Categories extends \yii\db\ActiveRecord {

    private $_fileName;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%categories}}';
    }

    public function behaviors() {
        return [
                'timestamp' => [
                    'class' => 'yii\behaviors\TimestampBehavior', 
                    'attributes' => [
                        \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'], 
                        \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ], 
                    'value' => new \yii\db\Expression('NOW()'),
                ], 
                'slug' => [
                    'class' => \yii\behaviors\SluggableBehavior::className(), 
                    'attribute' => 'title',
                    // 'slugAttribute' => 'slug',
                ], 
                'translateable' => [
                    'class' => \creocoder\translateable\TranslateableBehavior::className(),
                    'translationAttributes' => ['title', 'description'], 
                ], 
               ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['parent_id', 'position', 'is_active'], 'integer'], 
                [['title'], 'required'], 
                [['created_at', 'updated_at'], 'safe'],
                [['description'], 'string'], 
                [['title', 'slug'], 'string', 'max' => 200],
                [['slug'], 'unique'], 
                [['meta_title'], 'string', 'max' => 80], 
                [['meta_keywords'], 'string', 'max' => 150], 
                [['meta_description'], 'string', 'max' => 255], 
                [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'], 
               ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
                    'id' => Yii::t('category', 'ID'), 
                    'parent_id' => Yii::t('category', 'Parent category'), 
                    'title' => Yii::t('category', 'Title'), 
                    'slug' => Yii::t('category', 'Slug'), 
                    'description' => Yii::t('category', 'Description'), 
                    'image' => Yii::t('category', 'Category Image'), 
                    'meta_title' => Yii::t('category', 'Meta Title'), 
                    'meta_keywords' => Yii::t('category', 'Meta Keywords'), 
                    'meta_description' => Yii::t('category', 'Meta Description'), 
                    'position' => Yii::t('category', 'Position'), 
                    'is_active' => Yii::t('category', 'Status'), 
                    'created_at' => Yii::t('category', 'Created At'), 
                    'updated_at' => Yii::t('category', 'Updated At'),
               ];
    }

    public function getTtitle() {
        return ($this -> hasTranslation() ? $this -> translate() -> title : $this -> title);
    }

    public function getTdescription() {
        return ($this -> hasTranslation() ? $this -> translate() -> description : $this -> description);
    }

    public function getParentCategory() {
        return $this -> hasOne(self::className(), ['id' => 'parent_id']);
    }

    public function transactions() {
        return [self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE, ];
    }

    public function getTranslations() {
        return $this -> hasMany(CategoriesTranslation::className(), ['category_id' => 'id']);
    }

    public function getChildren() {
        return $this -> hasMany(self::className(), ['parent_id' => 'id']);
    }

    public static function createTreeList($parent_id, $current_category) {
        $categories = self::find() -> where(['parent_id' => $parent_id]) -> all();
        $parent_id = Yii::$app -> request -> getQueryParam('parent_id');
        $html = "";
        $html .= "<ul>";
        foreach ($categories as $category) {
            $html .= "<li>";
            $class = (($category -> id == $parent_id) || ($category -> id == $current_category)) ? "jstree-clicked" : "";
            $url = Yii::$app -> urlManager -> createUrl(['categories', 'id' => $category -> id]);
            $html .= "<a class=\"" . $class . "\" href=\"" . $url . "\">";
            $html .= Html::getAttributeValue($category, 'title');
            $html .= "</a>";
            $childCount = self::find() -> where(['parent_id' => $category -> id]) -> count();
            if ($childCount > 0) {
                $html .= self::createTreeList($category -> id, $current_category);
            }
            $html .= "</li>";
        }
        $html .= "</ul>";
        return $html;
    }

    public static function printEditPath($categoryId, $html = "") {
        $category = self::find() -> where(['id' => $categoryId]) -> one();
        if ($category -> parent_id != NULL) {
            $html .= self::printEditPath($category -> parent_id, $html);
        }
        $html .= "<a href=\"" . Yii::$app -> urlManager -> createUrl(['/categories', 'id' => $category -> id]) . "\">" . $category -> title . "</a> &nbsp;/ ";
        return $html;
    }

}
