<?php

namespace twofox\categories\models;

use Yii;

/**
 * This is the model class for table "{{%offers_translation}}".
 *
 * @property integer $offer_id
 * @property string $language
 * @property string $title
 * @property string $description
 *
 * @property Offers $offer
 */
class CategoriesTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'language', 'title'], 'required'],
            [['category_id'], 'integer'],
            [['description'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['title'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('offers', 'Category ID'),
            'language' => Yii::t('offers', 'Language'),
            'title' => Yii::t('offers', 'Title'),
            'description' => Yii::t('offers', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }
}
