<?php
/*
 * This file is part of the YiiModules.com
 *
 * (c) Yii2 modules open source project are hosted on <http://github.com/twofox/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
use yii\db\Migration;
use yii\db\Schema;

class m160207_131618_create_translate_categories_table extends Migration
{
    public function up()
    {
        $this->createTable('categories_translation', [
            'category_id'         => Schema::TYPE_PK,
            'title'   => Schema::TYPE_STRING . '(255) NOT NULL',
            'description' => Schema::TYPE_TEXT,
            'language' => Schema::TYPE_STRING . '(80) NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('categories');
    }
}
