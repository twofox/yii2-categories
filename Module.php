<?php
/*
 * This file is part of the YiiModules.com
 *
 * (c) Yii2 modules open source project are hosted on <http://github.com/twofox/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace twofox\categories;
use yii\helpers\Url;

/**
 * categories module definition class
 */
class Module extends \yii\base\Module {
    public $assets = "";
    private static $Categories = [];

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        $this -> setAliases(['@categories-assets' => __DIR__ . '/assets']);
        // custom initialization code goes here
        self::$Categories = \twofox\categories\models\Categories::find() -> where(['is_active' => 1, 'parent_id' => Null]) -> orderBy(['title' => 'ASC']) -> indexBy('id') -> all();
    }

    public function getAll($parent_id = NULL) {
        $arr = array();

        if (empty(self::$Categories))
            $data = self::$Categories;
        else if (!empty($parent_id) && !isset(self::$Categories[$parent_id]))
            return [];

        foreach (self::$Categories as $data) {
            $catData = ['url' => ['/category/default/index', 'id' => $data -> id, 'slug' => $data -> slug], 'label' => $data -> ttitle];

            if (isset($data -> children) && count($data -> children) > 0)
                $catData['items'] = self::getChildren($data);

            $arr[$data -> id] = $catData;
        }
        return $arr;
    }

    private function getChildren($model = null) {
        $items = [];
        if (isset($model -> children) && count($model -> children) > 0) {                
            foreach ($model->children as $key => $value) {

                if ($value -> is_active != 1)
                    continue;

                $items[$value -> id] = ['url' => ['/category/default/index', 'id' => $value -> id, 'slug' => $value -> slug], 'label' => $value -> ttitle];

                if (isset($value -> children) && count($value -> children) > 0)
                    $items[$value -> id]['items'] = self::getChildren($value);
            }
        }
        return $items;
    }

    private function cmp($f1, $f2) {
        if($f1->ttitle < $f2->ttitle) return -1;
        elseif($f1->ttitle > $f2->ttitle) return 1;
        else return 0;
    }

    public function getList($items_a = null, $prefix = '') {
        if ($items_a == null)
            $items_a = self::getAll();

        $items = [];
        foreach ($items_a as $item) {
            $items[$item['url']['id']] = trim($prefix . ' ' . $item['label']);

            if (isset($item['items']) && count($item['items']) > 0)
                $items += self::getList($item['items'], '—' . $prefix);
        }

        return $items;
    }

    public function getOne($category_id = Null) {
        if (!isset(self::$Categories[$category_id])) {
            return false;
        }

        $data = self::$Categories[$category_id];

        $catData = $data -> attributes;
        if (isset($data -> children) && count($data -> children) > 0) {
            $catData['sub_categories'][$value -> id] = $value -> attributes;
        }
        $arr[$data -> id] = $catData;
        return $arr;
    }

}
