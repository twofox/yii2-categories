<?php
/*
 * This file is part of the YiiModules.com
 *
 * (c) Yii2 modules open source project are hosted on <http://github.com/twofox/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
use twofox\categories\models\Categories;
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<div class="clearfix">&nbsp;</div>

<?= DetailView::widget([
	'model' => $model,
	'attributes' => [
		'id',
		[
			'attribute'=>'parent_id',
			'value' => Html::a(Html::getAttributeValue($model,'parentCategory[name]'),['/categories','id'=>$model->parent_id]),
			'format'=>'raw',
			'visible'=>($model->parent_id==0) ? false : true
		],
		'title',
		'slug',
		'description',
		'meta_title',
		'meta_keywords',
		'meta_description',
		[
			'attribute'=>'is_active',
			'value' => ($model->is_active==1) ? "Active" : "Inactive",
			'format'=>'raw',
		],
		//'is_active',
		'created_at:datetime',
		'updated_at:datetime'
		//'created_at',
		//'updated_at',
	],
]) ?>
